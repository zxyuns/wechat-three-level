# wechat-three-level
微信小程序,省市区三级联动

[原文地址：](https://blog.csdn.net/u012767761/article/details/90901892)
---- 说明：原文的代码基本没什么大问题，不过在选省市区的时候，由于用了input框，会导致手机的键盘弹出的问题。
      这里做了改动，采用view组件，可以解决这个问题。

![](https://www.edik.cn/upload/article/2017/1/10/6_1484036738156.gif)
